/*
  ID: kaviraj1
  TASK: packrec
  LANG: C++
*/

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstdlib>
#include <cstring>

using namespace std;

struct rect {
  int l,b;
};

bool bht[101];
int bestarea;

void record(rect big) {
  int a=big.l*big.b;

  //  cerr<<"Area: "<<a<<endl;

  if(a<bestarea || bestarea==0) {
    bestarea=a;
    memset(bht,false,sizeof(bht));
  }

  if(a==bestarea) {
    bht[min(big.l,big.b)]=true;
  }
}

void check(rect *r) {
  rect big;
  big.l=big.b=0;

  // case 1: all are lined up straight

  for(int i=0;i<4;++i) {
    big.l=max(big.l,r[i].l);
    big.b+=r[i].b;
  }
  record(big);

  big.l=big.b=0;

  // case 2: 0 to 2 lined up, 3 under

  for(int i=0;i<3;++i) {
    big.l=max(big.l,r[i].l);
    big.b+=r[i].b;
  }

  big.l+=r[3].l;
  big.b=max(big.b,r[3].b);

  record(big);

  big.l=big.b=0;

  //case 3: 0 and 1 lined up, 2 under 0 and 1. 3 lined up.

  big.b=r[0].b+r[1].b;
  big.b=max(big.b,r[2].b);
  big.b+=r[3].b;

  big.l=max(r[0].l,r[1].l);
  big.l+=r[2].l;
  big.l=max(big.l,r[3].l);

  record(big);

  big.l=big.b=0;

  // case 4 and 5: 1 stacked above 0, 2 and 3 lined up

  big.b=max(r[0].b,r[1].b);
  big.l=r[0].l+r[1].l;

  big.b+=(r[2].b+r[3].b);

  big.l=max(big.l,r[2].l);
  big.l=max(big.l,r[3].l);

  record(big);

  big.l=big.b=0;

  // case 6: packed one over the other line 2x2 matrix
  // eg:
  // 2 3
  // 0 1

  big.b=r[0].b+r[1].b;
  big.l=max(r[0].l+r[2].l,r[1].l+r[3].l);

  // 0 and 3 touch?
  if(r[0].l >  r[1].l)
    big.b=max(big.b,r[0].b+r[3].b);

  // 1 and 2 touch?
  if(r[1].l >  r[0].l)
    big.b=max(big.b,r[1].b+r[2].b);

  // 2 and 3 touch?
  if(r[0].l+r[2].l > r[1].l)
    big.b=max(big.b,r[2].b+r[3].b);

  big.b=max(big.b,r[2].b);
  big.b=max(big.b,r[3].b);
  record(big);
}

void rotate(rect *r,int n) {
  if(n==4) {
    check(r);
    return;
  }

  rotate(r,n+1);
  swap(r[n].l,r[n].b);
  rotate(r,n+1);
  swap(r[n].l,r[n].b);

}

void permutate(rect *r,int n) {
  if(n==4) {
    rotate(r,0);
  }

  for(int i=n;i<4;++i) {
    swap(r[i],r[n]);
    permutate(r,n+1);
    swap(r[i],r[n]);
  }
}

int main() {
  freopen("packrec.in","r",stdin);
  freopen("packrec.out","w",stdout);

  rect r[4];

  int l,b;
  for(int i=0;i<4;++i) {
    cin>>r[i].l>>r[i].b;
  }

  permutate(r,0);


  cout<<bestarea<<endl;
  cerr<<bestarea<<endl;
  
  for(int i=1;i<=100;++i) {
    if(bht[i]) {
      cout<<i<<" "<<bestarea/i<<endl;
      cerr<<i<<" "<<bestarea/i<<endl;
    }
  }

  return 0;
}

/*
  ID: kaviraj1
  LANG: C++
  TASK: milk2
*/

#include<iostream>
#include<cstdio>
#include<algorithm>

#define MAX 5000

using namespace std;

struct event {
  int start;
  int end;
};


bool cmp(const event &a,const event &b) {
  return a.start<b.start;
}

int main() {

  freopen("milk2.in","r",stdin);
  freopen("milk2.out","w",stdout);

  event milking[MAX];

  int N;
  cin>>N;
  for(int i=0;i<N;++i){
    cin>>milking[i].start;
    cin>>milking[i].end;
  }

  sort(milking,milking+N,cmp);

  //finding continues time;

  int con=0;
  int start,end;

  if(N==1) {
    con=milking[0].end-milking[0].start;
  }

  for(int i=0;i<N-1;++i) {
    start=milking[i].start;
    end=milking[i].end;

    while(milking[i].end > milking[i+1].start) {
      if(milking[i+1].end > milking[i].end)
	end=milking[i+1].end;
      i++;
    }

    con=max(con,end-start);

  }

  //finding idle time;

  int idle=0;

  int m=milking[0].end;

  for(int i=1;i<N;++i) {
    if(m < milking[i].start) {
      idle=max(idle,milking[i].start -m);
      m=milking[i].end;
    }
  }

  cout<<con<<" "<<idle<<endl;

  return 0;

}

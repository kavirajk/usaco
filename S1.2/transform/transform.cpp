/*
  ID: kaviraj1
  LANG: C++
  TASK: transform
*/
#include<iostream>
#include<cstdio>
#include<vector>
#include<cassert>
#include<iterator>

using namespace std;

bool operator==(const vector<string>& a,const vector<string>& b) {
  int len=a.size();
  string s1,s2;
  s1=s2="";
  for(int i=0;i<len;++i) {
    s1+=a[i];
    s2+=b[i];
  }
  return s1==s2;
}

vector<string> rotate(vector<string> src,int c) {
  int len=src.size();
  vector<string> dest=src;
  for(int k=0;k<c;++k) {
    for(int i=0;i<len;++i) {
      for(int j=0;j<len;++j) {
	dest[j][len-1-i]=src[i][j];
      }
    }
    src=dest;
  }
  return dest;
}

vector<string> reflect(vector<string> src) {
  vector<string> dest=src;
  int len=src.size();
  for(int i=0;i<len;++i) {
    for(int j=0;j<len/2;++j) {
      swap(dest[i][j],dest[i][len-1-j]);
    }
  }
  return dest;
}

int transform(vector<string> src,vector<string> dest) {
  if(dest==rotate(src,1))
    return 1;
  if(dest==rotate(src,2))
    return 2;
  if(dest==rotate(src,3))
    return 3;
  if(dest==reflect(src))
    return 4;
  if(dest==rotate(reflect(src),1) ||
     dest==rotate(reflect(src),2) ||
     dest==rotate(reflect(src),3))
    return 5;
  if(src==dest)
    return 6;
  return 7;
}

int main() {
  freopen("transform.in","r",stdin);
  freopen("transform.out","w",stdout);
  int N;
  cin>>N;
  vector<string>src(N),dest(N);
  for(int i=0;i<N;++i)
    cin>>src[i];
  for(int i=0;i<N;++i)
    cin>>dest[i];
  cout<<transform(src,dest)<<endl;
  return 0;
}

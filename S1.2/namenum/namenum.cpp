/*
  ID: kaviraj1
  TASK: namenum
  LANG: C++
*/
#include<iostream>
#include<cstdio>
#include<iterator>
#include<algorithm>
#include<vector>
#include<fstream>

using namespace std;

struct lmap {
  char c[3];
};

vector<string> res(531450);
int r_c;
string n;

lmap m[8]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','R','S','T','U','V','W','X','Y'};

void recur(int index,string r,int len) {
  //  cerr<<"inside recur\n";
  if(r.size()==len) {
    res[r_c]=r;
    ++r_c;
    return;
  } else {
    for(int i=0;i<3;++i) {
      recur(index+1,r+m[n[index]-'0'-2].c[i],len);
    }
  }
}

void generate(string src,int len) {
  string r="";
  recur(0,r,len);
}


int main() {
  ifstream dict("dict.txt");
  vector<string> v_dict;
  copy(istream_iterator<string>(dict),istream_iterator<string>(),back_inserter(v_dict));
  freopen("namenum.in","r",stdin);
  freopen("namenum.out","w",stdout);

  cin>>n;
  string src;
  src="";
  for(int i=0;i<n.size();++i) {
    for(int j=0;j<3;++j) {
      src+=m[n[i]-'0'-2].c[j];
    }
  }
  generate(src,n.size());
  //  copy(istream_iterator<string>(dict),istream_iterator<string>(),ostream_iterator<string>(cout,"\n"));
  //  copy(res.begin(),res.end(),ostream_iterator<string>(cout,"\n"));
  int c=0;
  for(int i=0;i<res.size();++i) {
    if(binary_search(v_dict.begin(),v_dict.end(),res[i])) {
      cout<<res[i]<<endl;
      c++;
    }
  }
  if(c==0)
    cout<<"NONE"<<endl;
  return 0;
}


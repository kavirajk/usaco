/*
  TASK: convert base 10 to any base using recursion
  Authour: kaviraj
*/

#include<iostream>
#include<cstdio>
#include<cstring>

using namespace std;

void to_base(char *s,int n,int b) {
  if(n==0) {
    strcpy(s,"");
    return;
  }

  to_base(s,n/b,b);

  int len=strlen(s);
  s[len]="0123456789ABCDEFGHIJ"[n%b];
  s[len+1]='\0';
}

int main() {
  int b,n;
  char s[20];
  cin>>b;
  while(true) {
    cin>>n;

    to_base(s,n,b);
    cout<<n<<": "<<s<<endl;
  }
  return 0;
}

/*
  ID: kaviraj1
  TASK: palsquare
  LANG: C++
*/

#include<iostream>
#include<cstdio>
#include<algorithm>

using namespace std;
bool isPalim(string s) {
  int low=0;
  int high=s.size()-1;
  while(low<high) {
    if(s[low]!=s[high])
      return false;
    ++low;
    --high;
  }
  return true;
}

string to_base(int n,int b) {
  string t="";
  while(n!=0) {
    t+="0123456789ABCDEFGHIJ"[n%b];
    n/=b;
  }
  reverse(t.begin(),t.end());
  return t;
}

int main() {

  freopen("palsquare.in","r",stdin);
  freopen("palsquare.out","w",stdout);

  int b;
  cin>>b;

  for(int i=1;i<=300;++i) {
    string num,num_sq;
    num_sq=to_base(i*i,b);
    if(isPalim(num_sq)) {
      num=to_base(i,b);
      cout<<num<<" "<<num_sq<<endl;
    }
  }

  return 0;
}

/*
  ID: kaviraj1
  TASK: dualpal
  LANG: C++
*/

#include<iostream>
#include<algorithm>
#include<cstdio>

using namespace std;

bool is_palim(string str) {

  int low=0;
  int high=str.size()-1;
  while(low<high) {
    if(str[low]!=str[high])
      return false;
    ++low;
    --high;
  }
  return true;
}

string to_base(int n,int b) {

  string t="";
  while(n!=0) {
    t+="0123456789A"[n%b];
    n/=b;
  }
  reverse(t.begin(),t.end());
  return t;
}

int main() {

  freopen("dualpal.in","r",stdin);
  freopen("dualpal.out","w",stdout);

  int n,s;
  cin>>n>>s;
  
  int total=0;
  int i=s+1;
  while(total<n) {
    int count=0;
    for(int b=2;b<=10;++b) {
      if(is_palim(to_base(i,b))) {
	cerr<<"numb: "<<to_base(i,b)<<endl;
	++count;
      }
    }
    if(count>=2){
      cout<<i<<endl;
      ++total;
    }
    ++i;
  }
  return 0;
}

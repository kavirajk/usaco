/*
  ID: kaviraj1
  LANG: C++
  TASK: beads
*/
#include<iostream>
#include<algorithm>
#include<cstdio>

using namespace std;

string beads;
int N;

int mod(int m,int n) {
  while(m<0)
    m+=n;
  return m%n;
}

int nbreak(int dir,int p) {

  if(dir<0)
    p=mod(p-1,N);

  int n;

  char color='w';

  for(n=0;n<N;n++,p=mod(p+dir,N)) {
    if(color=='w' && beads[p]!='w')
      color=beads[p];

    if(color!=beads[p] && beads[p]!='w')
      break;

  }

  return n;

}

int main(){

  freopen("beads.in","r",stdin);
  freopen("beads.out","w",stdout);

  cin>>N;
  cin>>beads;

  int m,n;

  m=0;
  for(int i=0;i<N;++i) {

    n=nbreak(1,i)+nbreak(-1,i);

    if(n>m)
      m=n;
  }

  if(m>N)
    m=N;

  cout<<m<<endl;

  return 0;

}

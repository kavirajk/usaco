/*
  ID: kaviraj1
  LANG: C++
  PROB: beads
*/

#include<iostream>
#include<cstdio>
#include<fstream>

using namespace std;

int main() {
  freopen("beads.in","r",stdin);
  freopen("beads.out","w",stdout);

  int N;
  cin>>N;
  string beadsP,beads;
  cin>>beadsP;

  int ans=0;

  for(int i=0;i<N;i++) {
    beads=beadsP;
    int leftB=0,rightB=0;
    char c=beads[i];

    bool vFix=false;
    //    cerr<<"------\nloop: "<<i<<endl;
    int left=i;

    if(beads[left]=='r' || beads[left]=='b')
      vFix=true;

    int right=(i+1)%N;

    //    cerr<<"left: "<<left<<" right: "<<right<<endl;

    int j=left;
    while(j!=right && (beads[j]==c || beads[j]=='w' || !vFix)) {
      if(beads[j]=='0')
	break;
      if(!vFix && (beads[j]=='r' || beads[j]=='b')) {
	vFix=true;
	c=beads[j];
      }

      //      cerr<<"beads: "<<beads[j]<<endl;
      beads[j]='0';
      ++leftB;
      j=(j+N-1)%N;
    }

    //    cerr<<"beads[j]: "<<beads[j]<<" c: "<<c<<endl;

    vFix=false;

    c=beads[right];
    j=right;

    if(beads[right]=='r' || beads[right]=='b')
      vFix=true;

    while(j!=left && (beads[j]==c || beads[j]=='w' || !vFix)) {

      if(beads[j]=='0')
	break;

      if(!vFix && (beads[j]=='r' || beads[j]=='b')) {
	vFix=true;
	c=beads[j];
      }

      beads[j]='0';
      ++rightB;
      j=(j+1)%N;
    }

    // if(i==23)
    cerr<<"i: "<<i<<" leftS: "<<leftB<<" rightB: "<<rightB<<endl;

    int tmp=(leftB+rightB);
    //    cout<<"tmp: "<<tmp<<endl;
    ans=max(ans,leftB+rightB);

  }
  cout<<ans<<endl;
  return 0;
}

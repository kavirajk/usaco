/*
 ID: kaviraj1
 LANG: C++
 PROG: friday
*/

#include<iostream>
#include<fstream>
#include<string>

using namespace std;

int N;
int day=3;
int count[7];
//int month[12];

bool isLeapYear(int n) {
  if(n%4==0 && n%100!=0)
    return true;
  if(n%400==0)
    return true;
  return false;
}

int main() {
  
  freopen("friday.in","r",stdin);
  freopen("friday.out","w",stdout);
  
  cin>>N;
  for(int y=1900;y<=1900+N-1;++y) {
    int month[]={31,28,31,30,31,30,31,31,30,31,30,31};
    if(isLeapYear(y))
      month[1]=29;

    for(int m=0;m<12;++m) {
      for(int d=1;d<=month[m];++d) {
	if(d==13){
	  //	  cout<<"reached date 13\n";
	  count[day-1]+=1;
	}
	day=(day%7)+1;
      }
    }
  }

  for(int i=0;i<7;i++) {
    cout<<count[i];
    if(i!=6)
      cout<<" ";
  }
  cout<<endl;

  return 0;

}

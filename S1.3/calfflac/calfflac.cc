/*
  ID: kaviraj1
  LANG: C++
  TASK: calfflac
*/

/* Desc: Palindrome can be thought of like a letter in a center and both sides of the center should be equal.
   the key idea here is instead of checking all the sequences of string as palindrome which is O(2^n),
   we can do the above check assuming every letter as center with time complexity O(n*m).

   there can be two variations. 1. center can be any letter ( odd palindrome), 2. center can be empty (even palindrome)
*/

#include <iostream>
#include <cstdio>

using namespace std;

string ls,_data,data;
int nlong,start,end;

int map[30000];

bool is_ignore(char c) {
  if((c<'a' || c>'z') && (c<'A' || c>'Z'))
    return true;
  return false;
}

void checkPalim(int left,int right) {
  string l="";
  int nl=0;
  while(left>=0 && right<_data.size()) {
    if(is_ignore(_data[left])) {
      cerr<<"dec left\n";
      left--;
    }
    if(is_ignore(_data[right])) {
      cerr<<"inc right\n";
      right++;
    }

    if(tolower(_data[left])!=tolower(_data[right])){
      break;
    }
    left--;
    right++;
  }

  cerr<<"left: "<<left<<" right: "<<right<<" length: "<<(right-left-1)<<endl;
  if(right-left-1>nlong) {
    nlong=right-left-1;
    start=map[left+1];
    end=map[right-1];
  }
}

int main() {
  freopen("calfflac.in","r",stdin);
  freopen("calfflac.out","w",stdout);

  string line;

  while(getline(cin,line)) {
    data+=(line)+"\n";
  }
  int k=0;
  for(int i=0;i<data.size();++i) {
    if((data[i]>='a' && data[i]<='z') || (data[i]>='A' && data[i]<='Z')) {
      _data+=tolower((data[i]));
      map[k++]=i;
    }
  }

  cerr<<data<<endl;

  for(int i=1;i<_data.size()-1;++i) {
    checkPalim(i-1,i+1); // odd palindrome
    checkPalim(i,i+1); // even palindrome
  }

  for(int i=start;i<=end;++i) {
    cout<<data[i];
  }
  cout<<endl;

  return 0;

}

/*
  ID: kaviraj1
  TASK: calfflac
  LANG: C++
*/

#include <iostream>
#include <cstdio>

using namespace std;

string _data,data;
int ln,start,end;
int map[30000];

void checkPalim(int left,int right) {
  while(left>=0 && right<_data.size()) {

    if(_data[left]!=_data[right])
      break;
    left--;
    right++;
  }

  if(right-left-1>ln) {
    ln=right-left-1;
    start=map[left+1];
    end=map[right-1];
  }
}

int main() {

  freopen("calfflac.in","r",stdin);
  freopen("calfflac.out","w",stdout);

  string line;
  while(getline(cin,line)) {
    data+=(line+'\n');
  }

  int k=0;

  for(int i=0;i<data.size();++i) {
    if((data[i] <='z' && data[i]>='a') || (data[i]<='Z' && data[i]>='A')) {
      _data+=(tolower(data[i]));
      map[k++]=i;
    }
  }
  //  cout<<"here\n";
  for(int i=1;i<_data.size()-1;++i) {
    checkPalim(i-1,i+1);
    checkPalim(i,i+1);
  }

  cout<<ln<<endl;

  if(ln==1)
    cout<<data[0]<<endl;
  else {
    for(int i=start;i<=end;++i) {
      cout<<data[i];
    }
  }
  cout<<endl;
  return 0;
}


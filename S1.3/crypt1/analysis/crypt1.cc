#include <iostream>
#include <cstdio>
#include <cstring>

using namespace std;

bool digits[10];

int n;

bool valid(int x,int p) {

  while(x) {
    if(!digits[x%10])
      return false;
    x/=10;
    p--;
  }

  //  cerr<<"p: "<<p<<endl;

  if(p==0)
    return true;
  
  return false;

}

bool isValidProd(int m,int n) {
  if(!valid(m*n,4) || !valid(m,3) || !valid(n,2))
    return false;

  while(n) {
    if(!valid(m*(n%10),3)) {
      //      cerr<<"returning false from while\n";
      return false;
    }
    n/=10;
  }
  //  cerr<<"returning true"<<endl;
  return true;
}

int main() {

  freopen("crypt1.in","r",stdin);
  freopen("crypt1.out","w",stdout);

  cin>>n;

  memset(digits,false,sizeof(digits));
  int t=0,sol=0;
  
  for(int i=0;i<n;++i) {
    cin>>t;
    digits[t]=true;
  }

  for(int i=100;i<1000;++i) {
    for(int j=10;j<100;++j) {
      if(isValidProd(i,j))
	sol++;
    }
  }
  cout<<sol<<endl;
  return 0;
}

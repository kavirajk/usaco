/*
  ID: kaviraj1
  TASK: crypt1
  LANG: C++
*/

#include <iostream>
#include <cstdio>
#include <algorithm>
#include <cstring>
#include <cassert>

using namespace std;

#define MAX 10

int n;
int ar[MAX];
bool isdig[MAX];

int len(int x) {
  int l=0;
  while(x){
    l++;
    x/=10;
  }
  return l;
}

bool digitsValid(int x) {
  while(x) {
    if(!isdig[x%10]) // check whether the digit x%10 is allowed
      //      cerr<<"returning false";
      return false;
    x/=10;
  }
  //  cerr<<"returning true";
  return true;
}

int pos(int x,int p) {
  
  assert(p<len(x));

  if(p==0)
    return x%10;
  if(p==1)
    return (x%100)/10;
  
}

int solve() {
  int ans=0;
  int one,two,three,four,five;
  one=two=three=four=five=0;

  for(int a=0;a<n;++a) {
    one+=(100*ar[a]);

    for(int b=0;b<n;++b) {
      one+=(10*ar[b]);

      for(int c=0;c<n;++c) {
	one+=ar[c];

	for(int d=0;d<n;++d) {
	  two+=(10*ar[d]);

	  for(int e=0;e<n;++e) {
	    two+=ar[e];

	    three=pos(two,0)*one;
	    four=pos(two,1)*one;

	    five=one*two;

	    if(digitsValid(three) && digitsValid(four) && digitsValid(five) && len(three)==3 && len(four)==3 && len(five)==4)
	      ans++;

	    two-=ar[e];
	  }

	  two=0;
	}

	one-=ar[c];
      }

      one-=(10*ar[b]);
    }

    one=0;
  }
  return ans;
}

int main() {
  
  memset(isdig,false,sizeof(isdig));
  
  freopen("crypt1.in","r",stdin);
  freopen("crypt1.out","w",stdout);

  cin>>n;

  for(int i=0;i<n;++i) {
    cin>>ar[i];
    isdig[ar[i]]=true;
  }

  cout<<solve()<<endl;

  return 0;

}

/*
  ID: kaviraj1
  LANG: C++
  TASK: milk
*/

#include<iostream>
#include<cstdio>
#include<cstring>

#define MAXPRICE 1001

using namespace std;

int m[MAXPRICE];

int main() {
  freopen("milk.in","r",stdin);
  freopen("milk.out","w",stdout);
  
  int milkers,need,price,amt;
  long ans=0;
  cin>>need>>milkers;

  for(int i=0;i<milkers;++i) {
    cin>>price;
    cin>>amt;
    //    cerr<<"price: "<<price<<" amt: "<<amt<<endl;
    m[price]+=amt;
    //    cerr<<"m["<<price<<"]: "<<m[price]<<endl;
  }

  for(int i=0;i<MAXPRICE && need; ++i) {
    if(need>=m[i]) {
      ans+=m[i]*i;
      //      cerr<<"i: "<<i<<" need: "<<need<<" m[i]: "<<m[i]<<endl;
      need-=m[i];
    } else {
      ans+=i*need;
      need=0;
    }
  }
  cout<<ans<<endl;
  return 0;
}

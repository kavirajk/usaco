/*
  ID: kaviraj1
  TASK: milk
  LANG: C++
*/

#include<iostream>
#include<cstdio>
#include<algorithm>

using namespace std;

struct milk {
  int price;
  int avail;
};

bool cmp(const milk& a,const milk& b) {
  return a.price < b.price;
}

int main() {

  freopen("milk.in","r",stdin);
  freopen("milk.out","w",stdout);

  milk m[5000];
  int needed,milkers;
  int ans=0;
  cin>>needed>>milkers;
  if(!needed || !milkers) {
    cout<<ans<<endl;
    return 0;
  }
  for(int i=0;i<milkers;++i) {
    cin>>m[i].price>>m[i].avail;
  }
  int i=0;
  sort(m,m+milkers,cmp);
  while(needed>=m[i].avail && i< milkers) {
    cerr<<"price: "<<m[i].price<<endl;
    needed-=m[i].avail;
    ans+=m[i].price*m[i].avail;
    ++i;
  }

  ans+=m[i].price*needed;
  cout<<ans<<endl;
  return 0;
}
